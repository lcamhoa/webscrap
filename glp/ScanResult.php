<?php
namespace Glp;
require_once 'ScanData.php';

class ScanResult {

	public $type;
	public $data;
	public $link;

	function __construct($type) {
		$this->type= $type;
		$this->data = array();
	}

	function addScanData($r) {
		array_push($this->data, $r);
	}

}
