<?php

require_once './lib/goutte.phar';
require_once './glp/ScanResult.php';
require_once './glp/ScanData.php';

use Goutte\Client;
use Glp\ScanResult;
use Glp\ScanData;
use GuzzleHttp\Client as GuzzleClient;

$URL_MAPS = array("yellowpages"=>"http://www.yellowpages.com/whitepages/address?street=%s&zip=%s&phone=%s",
					"yahoo"=>"https://search.yahoo.com/local/?p=%s&addr=%s %s");
define("YP_TYPE", "yellowpages");
define("YH_TYPE", "yahoo");

if ($_SERVER['REQUEST_METHOD'] === 'POST'  && in_array($_POST['type'], array(YP_TYPE, YH_TYPE) )  ) {
	$client = new Client();
	$guzzle = $client->getClient();
	$client->setClient(new GuzzleClient(array(
     'verify' => false
    ))); 
	
	$type = $_POST["type"];
	$addr = $_POST['address'];

	//$ypUrl = 'http://www.yellowpages.com/whitepages/address?street='.$addr.'&zip='.$_POST['zip'].'&phone='.$_POST['phone'];
	#http://www.yellowpages.com/whitepages/address?street=71+Fortune+Dr&zip=92618&phone=949-833-2221
	#https://search.yahoo.com/local/?p=Adidas&addr=71%20Fortune%20Dr%2C%2092618
	$r = new ScanResult($type);
	$url = '';
	if($type==YP_TYPE) {
		$addr = str_replace('','+',$addr);
		$url = sprintf($URL_MAPS["yellowpages"], $addr, $_POST["zip"], $_POST["phone"] );
		$ypCrawler = $client->request('GET', $url);
		$status_code = $client->getResponse()->getStatus();
		if($status_code==200){
			//process the documents
			$crawlerData = $ypCrawler->filter('div.phone-result')->each(function ($node) {
				$d = new ScanData();
				$a = $node->filter('a')->first();
				$d->name =  $a->text();
				$d->address = $node->filter('p.address')->first()->text();
				$d->phone = $node->filter('p.phone')->first()->text();
				$d->href = $a->attr('href');
				return $d;
			});
			foreach($crawlerData as  $v) {
				$r->addScanData($v);
			}
		}
	}elseif ($type == YH_TYPE) {
		$url = sprintf($URL_MAPS["yahoo"], $_POST["name"], $addr ,$_POST["zip"]);
		
		$yhCrawler = $client->request('GET', $url);
		$status_code = $client->getResponse()->getStatus();
		if($status_code==200){
			//process the documents
			$crawlerData = $yhCrawler->filter('ol.res li')->each(function ($node) {
				$d = new ScanData();
				$a = $node->filter('h4.title > a')->first();
				$d->name =  $a->text();
				$d->href = $a->attr('href');
				$d->address = $node->filter('div.addr div')->each(function($adr){ return $adr->text();});
				$d->phone = $node->filter('div.phone')->first()->text();
				return $d;
			});
			foreach($crawlerData as  $v) {
				$r->addScanData($v);
			}
		}
	}
	$r->link = $url;
	$data =  array('result' => $r);
	header('Content-Type: application/json');
	echo json_encode($data);
} else {
	header('Content-Type: application/json');
	echo json_encode('NOT-SUPPORT');
}
?>