<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Goldenlinkplus</title>
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600">
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="css/font-awesome.css" rel="stylesheet" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
</head>

<body>

<!--Top Section-->
<div class="container-fluid topsec">
		<div class="row">
			<div class="col-md-12 topbar">
				<div class="col-md-3 logo">
					<img src="images/logo.png" />
				</div>
				<div class="col-md-9 steps">
					<ul>
						<li><span class="rd_circle">1</span> Can Your Customers Find Your Business Online?</li>
						<li><span class="gry_circle">2</span> Check Search Engines For Errors</li>
						<li><span class="gry_circle">3</span> Update Your Business on 50 Sites</li>
						<li><span class="gry_circle">4</span> Fix All Errors Found!</li>
					</ul>
				</div>
			</div>
		</div>
</div>
<!--Top Section Ends-->

<!--Main Content-->
<div class="container col-md-12">
	<div class="col-md-12 main_cnt">
		<h1>Thousands of Customers Are Searching For Your Business Online, Right Now! <br />
		</h1>
		<div class="txt_gif">
			<img src="images/gif.gif" />
		</div>
		<div class="inner_cnt col-md-12">
			<h3>Use Gold’s <span class="red">FREE</span> Search Engine Optimizer below to see if your customers can <span class="black">EASILY</span> find your business online!</h3>
			<div class="col-md-2 lft_arrow">
				<img src="images/lft_arrow.jpg" />
			</div>
			<div class="col-md-8">
				<form class="main_frm" action="step2.php" method="post" >
					<div class="form-group">
						<div class="col-sm-12">
							<label for="Business Name">Business Name:</label>
							<input type="text" placeholder="5 Star Shipping" id="name" name="name" value='Adidas'  class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-8">
							<label for="Business Address">Business Address:</label>
							<input type="text" placeholder="71 Fortune Dr" id="address" value='71 Fortune Dr' name="address" class="form-control">
						</div>
						<div class="col-sm-4">
							<label for="">&nbsp;</label>
							<input type="text" placeholder="92618" id="zip" name="zip" value='92618' class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<label for="Business Phone">Business Phone:</label>
							<input type="text" placeholder="949-833-2221" id="phone" value='949-833-2221' name="phone"  class="form-control">
						</div>
					</div>
					<button class="btn btn-default frm_btn" type="submit">Optimize My Site Now!</button>
				</form>
			</div>
			<div class="col-md-2 rht_arrow">
				<img src="images/rht_arrow.jpg" />
			</div>
		</div>
	</div>
	<div class="container">
		<div class="chkout_info cent_mrg col-md-12">
		<div class="col-md-12 chkinfo lgt_gry">
			<span class="lv_gry">Live:</span> Uncensored, Uneditted, Unfiltered Customer Reviews 
		</div>
		<div class="col-md-12 bubble_rev brd_no">
			<div class="col-md-4">
				<p>11 minutes ago</p>
				<div class="talk-bubble tri-right round right-in">
				  <div class="talktext">
					<p>This is the second time i have ordered shirts and the customer service and the quality is awesome! Keep it up!!</p>
				  </div>
				</div>
				<span class="author">Shannon K. <br />Draper, UT</span>
			</div>
			<div class="col-md-4">
				<p>27 minutes ago</p>
				<div class="talk-bubble tri-right round right-in">
				  <div class="talktext">
					<p>This is the second time i have ordered shirts and the customer service and the quality is awesome! Keep it up!!</p>
				  </div>
				</div>
				<span class="author">Shannon K. <br />Draper, UT</span>
			</div>
			<div class="col-md-4">
				<p>1 hour 3 minutes ago</p>
				<div class="talk-bubble tri-right round right-in">
				  <div class="talktext">
					<p>This is the second time i have ordered shirts and the customer service and the quality is awesome! Keep it up!!</p>
				  </div>
				</div>
				<span class="author">Shannon K. <br />Draper, UT</span>
			</div>
		</div>
	</div>
		<div class="bubble_txt">
			<p class="triangle-right">With Gold, thousands of customers can <span class="red">easily</span> find your business everywhere, from Google, to Yahoo to Bing!</p>
			<img src="images/alllogos.jpg" />
		</div>
	</div>
</div>
<!--Main Content Ends-->

<!--Footer-->
<div class="container-fluid footer-warp">
	<div class="container">
		<p>Copyright 2015 © All Rights Reserved.</p>
	</div>
</div>
<!--Footer Ends-->







<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> 
<script src="js/bootstrap.js"></script> 
<script src="js/docs.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="js/ie10-viewport-bug-workaround.js"></script> 
</body>
</html>
